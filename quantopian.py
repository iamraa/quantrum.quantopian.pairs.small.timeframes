"""
Парный трейдинг. Уменьшенные таймфреймы: 1 час, 30 мин, 15 мин.

# 2016: EQY, T; DIA, SLB; E, MMP;
# 2015: DRE, O; CIT, STT; TLT, VNQ; PNR, FDX;
# 2014-2015: DRE, O; H, MMP; CMS, SPG;
    
# 2014: SMH, SSO
    
"""
import talib
import pandas as pd
import numpy as np
from statsmodels.tsa.stattools import adfuller


# подготовка теста
def initialize(context):
    # пара
    context.pair = [
        symbol('TLT'),
        symbol('VNQ')        
    ] 

# ежедневная проверка сигналов
def handle_data(context, data):
    # check time
    dt = get_datetime()
    # run every hour - dt.minute in [59]
    # run every 30 minutes - dt.minute in [59, 29]
    # run every 15 minutes - dt.minute in [59, 14, 29, 44]
    if dt.minute not in [59]:        
        return
    
    stocks = context.pair
    
    can_trade = data.can_trade(stocks)
    
    # выходим, если не можем торговать одной из акций
    for stock in stocks:
        if not can_trade[stock]:
            log.warn("Can't trade " + stock.symbol)
            return
    
    # загрузка исторических данных
    hist = data.history(stocks, ['close'], 500, '1d')
    hist_min = data.history(stocks, ['close'], 6000, '1m')

    tf = 'H'  # hour
    #tf = '30T'  # 30 minutes
    #tf = '15T'  # 15 minutes
    hist_small = dict()
    for stock in stocks:
        hist_small[stock] = hist_min['close'][stock].resample(tf).last().dropna()
    
    # переводим в относительные значения
    check_length = 120
    performance = [
        get_performance(hist['close'][stocks[0]][-check_length:].as_matrix()),
        get_performance(hist['close'][stocks[1]][-check_length:].as_matrix())
    ]    
    spread_performance = pd.Series(performance[0] - performance[1])
    
    # small timeframe
    perf_small = [
        get_performance(hist_small[stocks[0]][-check_length:].as_matrix()),
        get_performance(hist_small[stocks[1]][-check_length:].as_matrix())
    ]
    spread_perf_small = pd.Series(perf_small[0] - perf_small[1])
    
    # hour ln
    #spread_perf_ln = pd.Series(np.log(performance[0] / 100 + 1) - np.log(performance[1] / 100 + 1))
    #spread_perf_small_ln = pd.Series(np.log(perf_small[0] / 100 + 1) - np.log(perf_small[1] / 100 + 1))
    
    spread = spread_performance  # для проверки стационарности
    zscore = zscore_std(spread)  # standard
    
    #zscore = zscore_std(spread_perf_small)  # standard small
    #zscore = zscore_std(spread_perf_ln)  # ln
    #zscore = zscore_std(spread_perf_small_ln)  # ln small
    
    # сигнал
    z = zscore.iloc[-1]
    
    if len(spread[-check_length:].dropna()) < check_length:
        log.warn("Can't trade with short length")
        return
    
    # проверка стационарности
    result = adfuller(spread.iloc[-check_length:])
    score, pvalue, crit = result[0], result[1], result[4]
    coint = score < crit['5%']
    #coint = True  # проверка отключена
    
    if not coint:
        # если пропала стационарность, закрываемся
        order_target_percent(stocks[0], 0)
        order_target_percent(stocks[1], 0)
    elif z > 1:
        # если опережает акция А
        order_target_percent(stocks[0], -1)
        order_target_percent(stocks[1], 1)
    elif z < -1:
        # если опережает акция Б
        order_target_percent(stocks[0], 1)
        order_target_percent(stocks[1], -1)
    elif abs(z) < 0.05:
        # рядом с нулем закрываем позицию
        order_target_percent(stocks[0], 0)
        order_target_percent(stocks[1], 0)
    
    # собираем историю значений
    sign = abs(z) / z if z else 0
    record(**{
            'zscore': z if abs(z) <= 2.05 else sign*2.05,
            'z-perf': zscore_std(spread_performance).iloc[-1],
            stocks[0].symbol: context.portfolio.positions[stocks[0]].amount,
            stocks[1].symbol: context.portfolio.positions[stocks[1]].amount
          })

def zscore_std(series):
    return (series - series.mean()) / np.std(series)

def get_performance(a):
    """
    Convert vector to performance
    """
    a = a.round(2)
    return np.insert(np.cumsum(np.diff(a) / a[:-1] * 100.), 0, 0)

def get_diff(y):
    # Метод наименьших квадратов
    x = np.array([i for i in range(len(y))])
    arr = np.vstack([x, np.ones(len(x))]).T
    m, c = np.linalg.lstsq(arr, y)[0]
    #print(m, c)
    return c